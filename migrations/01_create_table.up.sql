--Cashbox
CREATE TABLE shifts(
    "id" UUID PRIMARY KEY,
    "shift_id" VARCHAR NOT NULL UNIQUE,
    "branch" UUID NOT NULL,
    "staff" UUID NOT NULL,
    "magazine" UUID NOT NULL,
    "status" VARCHAR DEFAULT 'opened',
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE transactions(
    "id" UUID PRIMARY KEY,
    "cash" INT,
    "uzcard" INT,
    "payme" INT,
    "click" INT,
    "humo" INT,
    "uzum" INT,
    "total_sum" INT,
    "shifts_id" UUID REFERENCES shifts("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE sales(
    "id" UUID PRIMARY KEY,
    "sale_id" VARCHAR UNIQUE NOT NULL,
    "shift_id" UUID REFERENCES shifts("id"),
    "branch" UUID NOT NULL,
    "magazine" UUID NOT NULL,
    "staff" UUID NOT NULL,
    "status" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE sale_products(
    "id" UUID PRIMARY KEY,
    "sale_id" UUID REFERENCES sales("id"),
    "brand" UUID NOT NULL,
    "category" UUID NOT NULL,
    "product_name" UUID NOT NULL,
    "barcode" VARCHAR NOT NULL UNIQUE,
    "left_quantity" INT,
    "quantity" INT,
    "price" INT,
    "total_price" INT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE payment(
    "id" UUID PRIMARY KEY,
    "cash" INT,
    "uzcard" INT,
    "payme" INT,
    "click" INT,
    "humo" INT,
    "uzum" INT,
    "visa" INT,
    "currency" VARCHAR,
    "exchange" INT,
    "total_sum" INT,
    "sales_id" UUID REFERENCES sales("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
