package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/cashbox_service/config"
	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/grpc/client"
	"gitlab.com/market_go/cashbox_service/pkg/logger"
	"gitlab.com/market_go/cashbox_service/storage"
)

type SaleProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*cashbox_service.UnimplementedSaleProductServiceServer
}

func NewSaleProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleProductService {
	return &SaleProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SaleProductService) Create(ctx context.Context, req *cashbox_service.SaleProductCreate) (*cashbox_service.SaleProduct, error) {
	u.log.Info("====== SaleProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create SaleProduct: u.strg.SaleProduct().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) GetById(ctx context.Context, req *cashbox_service.SaleProductPrimaryKey) (*cashbox_service.SaleProduct, error) {
	u.log.Info("====== SaleProduct Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Get By ID: u.strg.SaleProduct().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) GetList(ctx context.Context, req *cashbox_service.SaleProductGetListRequest) (*cashbox_service.SaleProductGetListResponse, error) {
	u.log.Info("====== SaleProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Get List: u.strg.SaleProduct().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) Update(ctx context.Context, req *cashbox_service.SaleProductUpdate) (*cashbox_service.SaleProduct, error) {
	u.log.Info("====== SaleProduct Update ======", logger.Any("req", req))

	resp, err := u.strg.SaleProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Update: u.strg.SaleProduct().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SaleProductService) Delete(ctx context.Context, req *cashbox_service.SaleProductPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== SaleProduct Delete ======", logger.Any("req", req))

	err := u.strg.SaleProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While SaleProduct Delete: u.strg.SaleProduct().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
