package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/cashbox_service/config"
	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/grpc/client"
	"gitlab.com/market_go/cashbox_service/pkg/logger"
	"gitlab.com/market_go/cashbox_service/storage"
)

type ShiftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*cashbox_service.UnimplementedShiftServiceServer
}

func NewShiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShiftService {
	return &ShiftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ShiftService) Create(ctx context.Context, req *cashbox_service.ShiftCreate) (*cashbox_service.Shift, error) {
	u.log.Info("====== Shift Create ======", logger.Any("req", req))

	resp, err := u.strg.Shift().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Shift: u.strg.Shift().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) GetById(ctx context.Context, req *cashbox_service.ShiftPrimaryKey) (*cashbox_service.Shift, error) {
	u.log.Info("====== Shift Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Shift().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Get By ID: u.strg.Shift().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) GetList(ctx context.Context, req *cashbox_service.ShiftGetListRequest) (*cashbox_service.ShiftGetListResponse, error) {
	u.log.Info("====== Shift Get List ======", logger.Any("req", req))

	resp, err := u.strg.Shift().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Get List: u.strg.Shift().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) Update(ctx context.Context, req *cashbox_service.ShiftUpdate) (*cashbox_service.Shift, error) {
	u.log.Info("====== Shift Update ======", logger.Any("req", req))

	resp, err := u.strg.Shift().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Update: u.strg.Shift().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShiftService) Delete(ctx context.Context, req *cashbox_service.ShiftPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Shift Delete ======", logger.Any("req", req))

	err := u.strg.Shift().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Shift Delete: u.strg.Shift().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
