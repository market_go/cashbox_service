package storage

import (
	"context"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
)

type StorageI interface {
	CloseDB()
	Shift() ShiftRepoI
	Transaction() TransactionRepoI
	Sale() SaleRepoI
	SaleProduct() SaleProductRepoI
	Payment() PaymentRepoI
}

type ShiftRepoI interface {
	Create(context.Context, *cashbox_service.ShiftCreate) (*cashbox_service.Shift, error)
	GetByID(context.Context, *cashbox_service.ShiftPrimaryKey) (*cashbox_service.Shift, error)
	GetList(context.Context, *cashbox_service.ShiftGetListRequest) (*cashbox_service.ShiftGetListResponse, error)
	Update(context.Context, *cashbox_service.ShiftUpdate) (*cashbox_service.Shift, error)
	Delete(context.Context, *cashbox_service.ShiftPrimaryKey) error
}

type TransactionRepoI interface {
	Create(context.Context, *cashbox_service.TransactionCreate) (*cashbox_service.Transaction, error)
	GetByID(context.Context, *cashbox_service.TransactionPrimaryKey) (*cashbox_service.Transaction, error)
	GetList(context.Context, *cashbox_service.TransactionGetListRequest) (*cashbox_service.TransactionGetListResponse, error)
	Update(context.Context, *cashbox_service.TransactionUpdate) (*cashbox_service.Transaction, error)
	Delete(context.Context, *cashbox_service.TransactionPrimaryKey) error
}

type SaleRepoI interface {
	Create(context.Context, *cashbox_service.SaleCreate) (*cashbox_service.Sale, error)
	GetByID(context.Context, *cashbox_service.SalePrimaryKey) (*cashbox_service.Sale, error)
	GetList(context.Context, *cashbox_service.SaleGetListRequest) (*cashbox_service.SaleGetListResponse, error)
	Update(context.Context, *cashbox_service.SaleUpdate) (*cashbox_service.Sale, error)
	Delete(context.Context, *cashbox_service.SalePrimaryKey) error
}

type SaleProductRepoI interface {
	Create(context.Context, *cashbox_service.SaleProductCreate) (*cashbox_service.SaleProduct, error)
	GetByID(context.Context, *cashbox_service.SaleProductPrimaryKey) (*cashbox_service.SaleProduct, error)
	GetList(context.Context, *cashbox_service.SaleProductGetListRequest) (*cashbox_service.SaleProductGetListResponse, error)
	Update(context.Context, *cashbox_service.SaleProductUpdate) (*cashbox_service.SaleProduct, error)
	Delete(context.Context, *cashbox_service.SaleProductPrimaryKey) error
}

type PaymentRepoI interface {
	Create(context.Context, *cashbox_service.PaymentCreate) (*cashbox_service.Payment, error)
	GetByID(context.Context, *cashbox_service.PaymentPrimaryKey) (*cashbox_service.Payment, error)
	GetList(context.Context, *cashbox_service.PaymentGetListRequest) (*cashbox_service.PaymentGetListResponse, error)
	Update(context.Context, *cashbox_service.PaymentUpdate) (*cashbox_service.Payment, error)
	Delete(context.Context, *cashbox_service.PaymentPrimaryKey) error
}
