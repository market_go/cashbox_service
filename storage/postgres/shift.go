package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/pkg/helper"
)

type ShiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) *ShiftRepo {
	return &ShiftRepo{
		db: db,
	}
}

func (r *ShiftRepo) Create(ctx context.Context, req *cashbox_service.ShiftCreate) (*cashbox_service.Shift, error) {

	count, err := r.GetList(ctx, &cashbox_service.ShiftGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id       = uuid.New().String()
		shift_id = helper.GenerateString("С", int(count.Count)+1)
		query    string
	)

	query = `
		INSERT INTO shifts(id, shift_id, branch, staff, magazine, updated_at)
		VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		shift_id,
		helper.NewNullString(req.Branch),
		helper.NewNullString(req.Staff),
		helper.NewNullString(req.Magazine),
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Shift{
		Id:       id,
		ShiftId:  shift_id,
		Branch:   req.Branch,
		Magazine: req.Magazine,
		Staff:    req.Staff,
	}, nil
}

func (r *ShiftRepo) GetByID(ctx context.Context, req *cashbox_service.ShiftPrimaryKey) (*cashbox_service.Shift, error) {

	var (
		query    string
		where    string
		argument string

		Id        sql.NullString
		ShiftId   sql.NullString
		Branch    sql.NullString
		Staff     sql.NullString
		Magazine  sql.NullString
		Status    sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	if len(req.StaffId) > 0 {
		where = " WHERE staff = $1"
		argument = req.StaffId
	} else if len(req.Id) > 0 {
		where = " WHERE id = $1"
		argument = req.Id
	}

	query = `
		SELECT
			id,
			shift_id,
			branch,
			staff,
			magazine,
			status,
			created_at,
			updated_at
		FROM shifts
	`
	query += where
	err := r.db.QueryRow(ctx, query, argument).Scan(
		&Id,
		&ShiftId,
		&Branch,
		&Staff,
		&Magazine,
		&Status,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Shift{
		Id:        Id.String,
		ShiftId:   ShiftId.String,
		Branch:    Branch.String,
		Staff:     Staff.String,
		Magazine:  Magazine.String,
		Status:    Status.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *ShiftRepo) GetList(ctx context.Context, req *cashbox_service.ShiftGetListRequest) (*cashbox_service.ShiftGetListResponse, error) {

	var (
		resp   = &cashbox_service.ShiftGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			shift_id,
			branch,
			staff,
			magazine,
			status,
			created_at,
			updated_at	
		FROM shifts
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchShiftId != "" {
		where += ` AND shift_id ILIKE '%' || '` + req.SearchShiftId + `' || '%'`
	}

	if req.SerchByBranchId != "" {
		where += ` AND branch::text ILIKE '%' || '` + req.SerchByBranchId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			ShiftId   sql.NullString
			Branch    sql.NullString
			Staff     sql.NullString
			Magazine  sql.NullString
			Status    sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&ShiftId,
			&Branch,
			&Staff,
			&Magazine,
			&Status,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Shiftes = append(resp.Shiftes, &cashbox_service.Shift{
			Id:        Id.String,
			ShiftId:   ShiftId.String,
			Branch:    Branch.String,
			Staff:     Staff.String,
			Magazine:  Magazine.String,
			Status:    Status.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ShiftRepo) Update(ctx context.Context, req *cashbox_service.ShiftUpdate) (*cashbox_service.Shift, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			shifts
		SET
			branch = :branch,
			staff = :staff,
			magazine = :magazine,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":       req.Id,
		"branch":   helper.NewNullString(req.Branch),
		"staff":    helper.NewNullString(req.Staff),
		"magazine": helper.NewNullString(req.Magazine),
		"status":   req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &cashbox_service.Shift{
		Id:       req.Id,
		Branch:   req.Branch,
		Staff:    req.Staff,
		Magazine: req.Magazine,
		Status:   req.Status,
	}, nil
}

func (r *ShiftRepo) Delete(ctx context.Context, req *cashbox_service.ShiftPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM shifts WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
