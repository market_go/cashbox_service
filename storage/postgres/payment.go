package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/pkg/helper"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) *PaymentRepo {
	return &PaymentRepo{
		db: db,
	}
}

func (r *PaymentRepo) Create(ctx context.Context, req *cashbox_service.PaymentCreate) (*cashbox_service.Payment, error) {

	var (
		id    = uuid.New().String()
		query string
	)
	req.TotalSum += (req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Uzum + req.Visa) * req.Exchange
	query = `
		INSERT INTO payment(id, cash, uzcard, payme, click, humo, uzum, visa, currency, exchange, total_sum, sales_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Uzum,
		req.Visa,
		req.Currency,
		req.Exchange,
		req.TotalSum,
		req.SalesId,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Payment{
		Id:       id,
		Cash:     req.Cash,
		Uzcard:   req.Uzcard,
		Payme:    req.Payme,
		Click:    req.Click,
		Humo:     req.Humo,
		Uzum:     req.Uzum,
		Visa:     req.Visa,
		Currency: req.Currency,
		Exchange: req.Exchange,
		TotalSum: req.TotalSum,
		SalesId:  req.SalesId,
	}, nil
}

func (r *PaymentRepo) GetByID(ctx context.Context, req *cashbox_service.PaymentPrimaryKey) (*cashbox_service.Payment, error) {

	var (
		query string
		where string
		args  string

		Id        sql.NullString
		Cash      sql.NullInt64
		Uzcard    sql.NullInt64
		Payme     sql.NullInt64
		Click     sql.NullInt64
		Humo      sql.NullInt64
		Uzum      sql.NullInt64
		Visa      sql.NullInt64
		Currency  sql.NullString
		Exchange  sql.NullInt64
		TotalSum  sql.NullInt64
		SalesId   sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)
	if len(req.Id) > 0 {
		where = " WHERE id = $1"
		args = req.Id
	} else if len(req.SaleId) > 0 {
		where = " WHERE sales_id = $1"
		args = req.SaleId
	}

	query = `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			uzum,
			visa,
			currency,
			exchange,
			total_sum,
			sales_id,
			created_at,
			updated_at
		FROM payment
	`
	query += where
	err := r.db.QueryRow(ctx, query, args).Scan(
		&Id,
		&Cash,
		&Uzcard,
		&Payme,
		&Click,
		&Humo,
		&Uzum,
		&Visa,
		&Currency,
		&Exchange,
		&TotalSum,
		&SalesId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Payment{
		Id:        Id.String,
		Cash:      Cash.Int64,
		Uzcard:    Uzcard.Int64,
		Payme:     Payme.Int64,
		Click:     Click.Int64,
		Humo:      Humo.Int64,
		Uzum:      Uzum.Int64,
		Visa:      Visa.Int64,
		Currency:  Currency.String,
		Exchange:  Exchange.Int64,
		TotalSum:  TotalSum.Int64,
		SalesId:   SalesId.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *PaymentRepo) GetList(ctx context.Context, req *cashbox_service.PaymentGetListRequest) (*cashbox_service.PaymentGetListResponse, error) {

	var (
		resp   = &cashbox_service.PaymentGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			uzum,
			visa,
			currency,
			exchange,
			total_sum,
			sales_id,
			created_at,
			updated_at	
		FROM payment
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchSalesId != "" {
		where += ` AND sales_id ILIKE '%' || '` + req.SearchSalesId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			Cash      sql.NullInt64
			Uzcard    sql.NullInt64
			Payme     sql.NullInt64
			Click     sql.NullInt64
			Humo      sql.NullInt64
			Uzum      sql.NullInt64
			Visa      sql.NullInt64
			Currency  sql.NullString
			Exchange  sql.NullInt64
			TotalSum  sql.NullInt64
			SalesId   sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Cash,
			&Uzcard,
			&Payme,
			&Click,
			&Humo,
			&Uzum,
			&Visa,
			&Currency,
			&Exchange,
			&TotalSum,
			&SalesId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Payments = append(resp.Payments, &cashbox_service.Payment{
			Id:        Id.String,
			Cash:      Cash.Int64,
			Uzcard:    Uzcard.Int64,
			Payme:     Payme.Int64,
			Click:     Click.Int64,
			Humo:      Humo.Int64,
			Uzum:      Uzum.Int64,
			Visa:      Visa.Int64,
			Currency:  Currency.String,
			Exchange:  Exchange.Int64,
			TotalSum:  TotalSum.Int64,
			SalesId:   SalesId.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *PaymentRepo) Update(ctx context.Context, req *cashbox_service.PaymentUpdate) (*cashbox_service.Payment, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			payment
		SET
			cash = :cash,
			uzcard = :uzcard,
			payme = :payme,
			click = :click,
			humo = :humo,
			uzum = :uzum,
			visa = :visa,
			currency = :currency,
			exchange = :exchange,
			total_sum = :total_sum,
			sales_id = :sales_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.Id,
		"cash":      req.Cash,
		"uzcard":    req.Uzcard,
		"payme":     req.Payme,
		"click":     req.Click,
		"humo":      req.Humo,
		"uzum":      req.Uzum,
		"visa":      req.Visa,
		"currency":  req.Currency,
		"exchange":  req.Exchange,
		"total_sum": req.TotalSum,
		"sales_id":  req.SalesId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &cashbox_service.Payment{
		Id:       req.Id,
		Cash:     req.Cash,
		Uzcard:   req.Uzcard,
		Payme:    req.Payme,
		Click:    req.Click,
		Humo:     req.Humo,
		Uzum:     req.Uzum,
		Visa:     req.Visa,
		Currency: req.Currency,
		Exchange: req.Exchange,
		TotalSum: req.TotalSum,
		SalesId:  req.SalesId,
	}, nil
}

func (r *PaymentRepo) Delete(ctx context.Context, req *cashbox_service.PaymentPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM payment WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
