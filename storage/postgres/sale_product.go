package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/pkg/helper"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *SaleProductRepo {
	return &SaleProductRepo{
		db: db,
	}
}

func (r *SaleProductRepo) Create(ctx context.Context, req *cashbox_service.SaleProductCreate) (*cashbox_service.SaleProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO sale_products(id, brand, category, product_name, barcode, left_quantity, quantity, price, total_price, sale_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Brand,
		req.Category,
		req.ProductName,
		req.Barcode,
		req.LeftQuantity,
		req.Quantity,
		req.Price,
		req.TotalPrice,
		req.SaleId,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.SaleProduct{
		Id:           id,
		SaleId:       req.SaleId,
		Brand:        req.Brand,
		Category:     req.Category,
		ProductName:  req.ProductName,
		Barcode:      req.Barcode,
		LeftQuantity: req.LeftQuantity,
		Quantity:     req.Quantity,
		Price:        req.Price,
		TotalPrice:   req.TotalPrice,
	}, nil
}

func (r *SaleProductRepo) GetByID(ctx context.Context, req *cashbox_service.SaleProductPrimaryKey) (*cashbox_service.SaleProduct, error) {

	var (
		query    string
		where    string
		argument string

		Id           sql.NullString
		SaleId       sql.NullString
		Brand        sql.NullString
		Category     sql.NullString
		ProductName  sql.NullString
		Barcode      sql.NullString
		LeftQuantity sql.NullInt64
		Quantity     sql.NullInt64
		Price        sql.NullInt64
		TotalPrice   sql.NullInt64
		CreatedAt    sql.NullString
		UpdatedAt    sql.NullString
	)

	if len(req.Barcode) > 0 {
		where = " WHERE barcode = $1"
		argument = req.Barcode
	} else if len(req.Id) > 0 {
		where = " WHERE id = $1"
		argument = req.Id
	}

	query = `
		SELECT
			id,
			sale_id,
			brand,
			category,
			product_name,
			barcode,
			left_quantity,
			quantity,
			price,
			total_price,
			created_at,
			updated_at
		FROM sale_products
	`
	query += where
	err := r.db.QueryRow(ctx, query, argument).Scan(
		&Id,
		&SaleId,
		&Brand,
		&Category,
		&ProductName,
		&Barcode,
		&LeftQuantity,
		&Quantity,
		&Price,
		&TotalPrice,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.SaleProduct{
		Id:           Id.String,
		SaleId:       SaleId.String,
		Brand:        Brand.String,
		Category:     Category.String,
		ProductName:  ProductName.String,
		Barcode:      Barcode.String,
		LeftQuantity: LeftQuantity.Int64,
		Quantity:     Quantity.Int64,
		Price:        Price.Int64,
		TotalPrice:   TotalPrice.Int64,
		CreatedAt:    CreatedAt.String,
		UpdatedAt:    UpdatedAt.String,
	}, nil
}

func (r *SaleProductRepo) GetList(ctx context.Context, req *cashbox_service.SaleProductGetListRequest) (*cashbox_service.SaleProductGetListResponse, error) {

	var (
		resp   = &cashbox_service.SaleProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			brand,
			category,
			product_name,
			barcode,
			left_quantity,
			quantity,
			price,
			total_price,
			created_at,
			updated_at	
		FROM sale_products
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchBySaleId != "" {
		where += ` AND sale_id::text ILIKE '%' || '` + req.SearchBySaleId + `' || '%'`
	}

	if req.SearchByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SearchByBarcode + `' || '%'`
	}

	if req.SearchByBrand != "" {
		where += ` AND brand ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	query += where + offset + limit
	fmt.Println(query)
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id           sql.NullString
			SaleId       sql.NullString
			Brand        sql.NullString
			Category     sql.NullString
			ProductName  sql.NullString
			Barcode      sql.NullString
			LeftQuantity sql.NullInt64
			Quantity     sql.NullInt64
			Price        sql.NullInt64
			TotalPrice   sql.NullInt64
			CreatedAt    sql.NullString
			UpdatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&SaleId,
			&Brand,
			&Category,
			&ProductName,
			&Barcode,
			&LeftQuantity,
			&Quantity,
			&Price,
			&TotalPrice,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.SaleProductes = append(resp.SaleProductes, &cashbox_service.SaleProduct{
			Id:           Id.String,
			SaleId:       SaleId.String,
			Brand:        Brand.String,
			Category:     Category.String,
			ProductName:  ProductName.String,
			Barcode:      Barcode.String,
			LeftQuantity: LeftQuantity.Int64,
			Quantity:     Quantity.Int64,
			Price:        Price.Int64,
			TotalPrice:   TotalPrice.Int64,
			CreatedAt:    CreatedAt.String,
			UpdatedAt:    UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleProductRepo) Update(ctx context.Context, req *cashbox_service.SaleProductUpdate) (*cashbox_service.SaleProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			sale_products
		SET
			sale_id = :sale_id,
			brand = :brand,
			category = :category,
			product_name = :product_name,
			barcode = :barcode,
			left_quantity = :left_quantity,
			quantity = :quantity,
			price = :price,
			total_price = :total_price,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.Id,
		"sale_id":       helper.NewNullString(req.SaleId),
		"brand":         helper.NewNullString(req.Brand),
		"category":      helper.NewNullString(req.Category),
		"product_name":  helper.NewNullString(req.ProductName),
		"barcode":       req.Barcode,
		"left_quantity": req.LeftQuantity,
		"quantity":      req.Quantity,
		"price":         req.Price,
		"total_price":   req.TotalPrice,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &cashbox_service.SaleProduct{
		Id:           req.Id,
		SaleId:       req.SaleId,
		Brand:        req.Brand,
		Category:     req.Category,
		ProductName:  req.ProductName,
		Barcode:      req.Barcode,
		LeftQuantity: req.LeftQuantity,
		Quantity:     req.Quantity,
		Price:        req.Price,
		TotalPrice:   req.TotalPrice,
	}, nil
}

func (r *SaleProductRepo) Delete(ctx context.Context, req *cashbox_service.SaleProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sale_products WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
