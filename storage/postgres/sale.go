package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/pkg/helper"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *SaleRepo {
	return &SaleRepo{
		db: db,
	}
}

func (r *SaleRepo) Create(ctx context.Context, req *cashbox_service.SaleCreate) (*cashbox_service.Sale, error) {

	count, err := r.GetList(ctx, &cashbox_service.SaleGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id      = uuid.New().String()
		sale_id = helper.GenerateString(string(req.SaleId[0]), int(count.Count)+1)
		query   string
	)

	query = `
		INSERT INTO sales(id, sale_id, shift_id, branch, magazine, staff, status, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		sale_id,
		helper.NewNullString(req.ShiftId),
		helper.NewNullString(req.Branch),
		helper.NewNullString(req.Magazine),
		helper.NewNullString(req.Staff),
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Sale{
		Id:       id,
		SaleId:   sale_id,
		ShiftId:  req.ShiftId,
		Branch:   req.Branch,
		Magazine: req.Magazine,
		Staff:    req.Staff,
		Status:   req.Status,
	}, nil
}

func (r *SaleRepo) GetByID(ctx context.Context, req *cashbox_service.SalePrimaryKey) (*cashbox_service.Sale, error) {

	var (
		query string

		Id        sql.NullString
		SaleId    sql.NullString
		ShiftId   sql.NullString
		Branch    sql.NullString
		Magazine  sql.NullString
		Staff     sql.NullString
		Status    sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			sale_id,
			shift_id,
			branch,
			magazine,
			staff,
			status,
			created_at,
			updated_at
		FROM sales
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&SaleId,
		&ShiftId,
		&Branch,
		&Magazine,
		&Staff,
		&Status,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Sale{
		Id:        Id.String,
		SaleId:    SaleId.String,
		ShiftId:   ShiftId.String,
		Branch:    Branch.String,
		Staff:     Staff.String,
		Magazine:  Magazine.String,
		Status:    Status.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *SaleRepo) GetList(ctx context.Context, req *cashbox_service.SaleGetListRequest) (*cashbox_service.SaleGetListResponse, error) {

	var (
		resp   = &cashbox_service.SaleGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			shift_id,
			branch,
			magazine,
			staff,
			status,
			created_at,
			updated_at	
		FROM sales
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	if req.SearchBySaleId != "" {
		where += ` AND sale_id ILIKE '%' || '` + req.SearchBySaleId + `' || '%'`
	}

	if req.SearchByShiftId != "" {
		where += ` AND shift_id ILIKE '%' || '` + req.SearchByShiftId + `' || '%'`
	}

	if req.SearchByStaff != "" {
		where += ` AND staff ILIKE '%' || '` + req.SearchByStaff + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			SaleId    sql.NullString
			ShiftId   sql.NullString
			Branch    sql.NullString
			Magazine  sql.NullString
			Staff     sql.NullString
			Status    sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&SaleId,
			&ShiftId,
			&Branch,
			&Magazine,
			&Staff,
			&Status,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Salees = append(resp.Salees, &cashbox_service.Sale{
			Id:        Id.String,
			SaleId:    SaleId.String,
			ShiftId:   ShiftId.String,
			Branch:    Branch.String,
			Staff:     Staff.String,
			Magazine:  Magazine.String,
			Status:    Status.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SaleRepo) Update(ctx context.Context, req *cashbox_service.SaleUpdate) (*cashbox_service.Sale, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			sales
		SET
			shift_id = :shift_id,
			branch = :branch,
			magazine = :magazine,
			staff = :staff,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":       req.Id,
		"shift_id": helper.NewNullString(req.ShiftId),
		"branch":   helper.NewNullString(req.Branch),
		"magazine": helper.NewNullString(req.Magazine),
		"staff":    helper.NewNullString(req.Staff),
		"status":   req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &cashbox_service.Sale{
		Id:       req.Id,
		ShiftId:  req.ShiftId,
		Branch:   req.Branch,
		Magazine: req.Magazine,
		Staff:    req.Staff,
		Status:   req.Status,
	}, nil
}

func (r *SaleRepo) Delete(ctx context.Context, req *cashbox_service.SalePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sales WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
