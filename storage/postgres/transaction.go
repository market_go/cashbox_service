package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/cashbox_service/genproto/cashbox_service"
	"gitlab.com/market_go/cashbox_service/pkg/helper"
)

type TransactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) *TransactionRepo {
	return &TransactionRepo{
		db: db,
	}
}

func (r *TransactionRepo) Create(ctx context.Context, req *cashbox_service.TransactionCreate) (*cashbox_service.Transaction, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO transactions(id, cash, uzcard, payme, click, humo, uzum, total_sum, shifts_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Uzum,
		req.TotalSum,
		helper.NewNullString(req.ShiftId),
	)

	if err != nil {
		return nil, err
	}

	return &cashbox_service.Transaction{
		Id:       id,
		Cash:     req.Cash,
		Uzcard:   req.Uzcard,
		Payme:    req.Payme,
		Click:    req.Click,
		Humo:     req.Humo,
		Uzum:     req.Uzum,
		TotalSum: req.TotalSum,
		ShiftId:  req.ShiftId,
	}, nil
}

func (r *TransactionRepo) GetByID(ctx context.Context, req *cashbox_service.TransactionPrimaryKey) (*cashbox_service.Transaction, error) {

	var (
		query string

		Id        sql.NullString
		Cash      sql.NullInt64
		Uzcard    sql.NullInt64
		Payme     sql.NullInt64
		Click     sql.NullInt64
		Humo      sql.NullInt64
		Uzum      sql.NullInt64
		TotalSum  sql.NullInt64
		ShiftId   sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)
	query = `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			uzum,
			total_sum,
			shifts_id,
			created_at,
			updated_at
		FROM transactions
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Cash,
		&Uzcard,
		&Payme,
		&Click,
		&Humo,
		&Uzum,
		&TotalSum,
		&ShiftId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}
	return &cashbox_service.Transaction{
		Id:        Id.String,
		Cash:      Cash.Int64,
		Uzcard:    Uzcard.Int64,
		Payme:     Payme.Int64,
		Click:     Click.Int64,
		Humo:      Humo.Int64,
		Uzum:      Uzum.Int64,
		TotalSum:  TotalSum.Int64,
		ShiftId:   ShiftId.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *TransactionRepo) GetList(ctx context.Context, req *cashbox_service.TransactionGetListRequest) (*cashbox_service.TransactionGetListResponse, error) {

	var (
		resp   = &cashbox_service.TransactionGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			uzum,
			total_sum,
			shifts_id,
			created_at,
			updated_at	
		FROM transactions
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchShiftId != "" {
		where += ` AND shifts_id ILIKE '%' || '` + req.SearchShiftId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			Cash      sql.NullInt64
			Uzcard    sql.NullInt64
			Payme     sql.NullInt64
			Click     sql.NullInt64
			Humo      sql.NullInt64
			Uzum      sql.NullInt64
			TotalSum  sql.NullInt64
			ShiftId   sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Cash,
			&Uzcard,
			&Payme,
			&Click,
			&Humo,
			&Uzum,
			&TotalSum,
			&ShiftId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Transactions = append(resp.Transactions, &cashbox_service.Transaction{
			Id:        Id.String,
			Cash:      Cash.Int64,
			Uzcard:    Uzcard.Int64,
			Payme:     Payme.Int64,
			Click:     Click.Int64,
			Humo:      Humo.Int64,
			Uzum:      Uzum.Int64,
			TotalSum:  TotalSum.Int64,
			ShiftId:   ShiftId.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *TransactionRepo) Update(ctx context.Context, req *cashbox_service.TransactionUpdate) (*cashbox_service.Transaction, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			transactions
		SET
			cash = :cash,
			uzcard = :uzcard,
			payme = :payme,
			click = :click,
			humo = :humo,
			uzum = :uzum,
			total_sum = :total_sum,
			shifts_id = :shifts_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.Id,
		"cash":      req.Cash,
		"uzcard":    req.Uzcard,
		"payme":     req.Payme,
		"click":     req.Click,
		"humo":      req.Humo,
		"uzum":      req.Uzum,
		"total_sum": req.TotalSum,
		"shifts_id": helper.NewNullString(req.ShiftId),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &cashbox_service.Transaction{
		Id:       req.Id,
		Cash:     req.Cash,
		Uzcard:   req.Uzcard,
		Payme:    req.Payme,
		Click:    req.Click,
		Humo:     req.Humo,
		Uzum:     req.Uzum,
		TotalSum: req.TotalSum,
		ShiftId:  req.ShiftId,
	}, nil
}

func (r *TransactionRepo) Delete(ctx context.Context, req *cashbox_service.TransactionPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM transactions WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
